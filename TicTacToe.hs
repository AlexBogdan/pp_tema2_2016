{-# LANGUAGE MultiParamTypeClasses, TypeSynonymInstances, TupleSections #-}
{-# OPTIONS_GHC -fno-warn-tabs #-}

module TicTacToe where

import MCTS()
import GameState

import System.Random

{-
	Tipul celulelor (1-9)
-}
type Cell = Int

{-
	Tipul jucătorilor
-}
data Player = X | O
	deriving (Eq, Enum, Show)

{-
	Întoarce celălalt jucător.
-}
otherPlayer :: Player -> Player
otherPlayer X = O
otherPlayer O = X

{-
	*** TODO ***

	Tipul stării jocului. Ar trebui să conțină informații despre tablă
	și despre jucătorul care urmează să mute.
-}
data Board = Board {
	cells :: [Maybe Player],
	currentPlayer :: Player
} deriving Eq

{-
	*** TODO ***

	Întoarce lista conținuturilor celulelor, unde celule libere
	sunt reprezentate de `Nothing`.

	Ordinea celulelor este următoarea:

	789
	456
	123
-}
boardConfiguration :: Board -> [Maybe Player]
boardConfiguration board = cells board

{-
	*** TODO ***

	Întoarce jucătorul care urmează să mute.
-}
boardPlayer :: Board -> Player
boardPlayer board = currentPlayer board

{-
	*** TODO ***

	Instanțiați clasa `Show` cu tipul `Board`.
-}
instance Show Board where
	show b = show [(map translate b1), (map translate b2), (map translate b3)]
		where 
			translate player = case player of
					Just X -> 'X'
					Just O -> 'O'
					Nothing -> 'N'
			b1 = drop 6 $ boardConfiguration b
			b2 = take 3 $ drop 3 $ boardConfiguration b
			b3 = take 3 $ boardConfiguration b


{-
	*** TODO ***

	Instanțiați clasa `GameBoard` cu tipurile `Board` și `Cell`.
-}
instance GameState Board Cell where
	-- playerIndex :: Board -> Int
	playerIndex board = case (boardPlayer board) of
			X -> 1
			O -> 0

	-- maxPlayers :: Board -> Int
	maxPlayers board = 2

	-- successors :: Board -> [(Cell, Board)]
	successors board = case outcome board of
		Ongoing -> map checkCell [1..9]
		otherwise -> []
		where
			checkCell x = case (boardConfiguration board)!!(x-1) of
				Nothing -> case place x board of
					Just b -> (x, b)

	-- outcome :: Board -> Outcome
	outcome b = do
		let board = boardConfiguration b
			-- isOver :: Board -> Bool
		let isOver = null $ filter (\x -> x == Nothing) board
			-- Verifica daca o lista de 3 elemente reprezinta un castigator
			-- winner :: [Maybe Player] -> Bool
		let winner list = (list !! 0 /= Nothing) &&
						   (list !! 0 == list !! 1) &&
						   (list !! 0 == list !! 2)
			-- Verifica fiecare linie in parte
			-- lineCheck :: [Maybe Player] -> Maybe Player
		let lineCheck = case winner $ take 3 board of
			True -> head $ take 3 board
			False -> case winner $ take 3 $ drop 3 board of
				True -> head $ take 3 $ drop 3 board
				False -> case winner $ drop 6 board of
					True -> head $ drop 3 board
					False -> Nothing
			-- Verifica fiecare coloana in parte
			-- lineCheck :: [Maybe Player] -> Maybe Player
		let columnChek = case winner $ [board !! 0, board !! 3, board !! 6] of
			True -> head board
			False -> case winner $ [board !! 1, board !! 4, board !! 7] of
				True -> head $ drop 1 board
				False -> case winner $ [board !! 2, board !! 5, board !! 8] of
					True -> last board
					False -> Nothing
			-- Verifica fiecare diagonala in parte
			-- lineCheck :: [Maybe Player] -> Maybe Player
		let diagonalCheck = case winner $ [board !! 0, board !! 4, board !! 8] of 
			True -> head board
			False -> case winner $ [board !! 2, board !! 4 , board !! 6] of
				True -> head $ drop 2 board
				False -> Nothing
			-- Verifica daca jocul este terminat si intoarce semnificatia corespunzatoare
		case lineCheck of
			Just X -> Win 1
			Just O -> Win (-1)
			Nothing -> case columnChek of
				Just X -> Win 1
				Just O -> Win (-1)
				Nothing -> case diagonalCheck of
					Just X -> Win 1
					Just O -> Win (-1)
					Nothing -> case isOver of
						True -> Draw 0
						False -> Ongoing
{-
	*** TODO ***

	Tabla inițială de joc. X mută primul.
-}
initialBoard :: Board
initialBoard = Board [Nothing, Nothing, Nothing,
					  Nothing, Nothing, Nothing,
					  Nothing, Nothing, Nothing]
					 X

{-
	*** TODO ***

	Mută în celula dată ca parametru, în funcție de jucătorul aflat la rând,
	și schimbă jucătorul curent.

	Ordinea celulelor este explicată la funcția `boardConfiguration`.
-}
place :: Cell -> Board -> Maybe Board
place c b = case cell of
				Nothing -> Just $ Board ((take (c-1) board) ++ [Just player] ++ (drop c board)) (otherPlayer player)
				otherwise -> Nothing
			where
				board = boardConfiguration b
				player = boardPlayer b
				cell = head $ drop (c-1) board



{-
	*** TODO ***

	Alege o mutare pornind din starea curentă.

	Utilizați `choose` din modulul `MCTS`, cu un număr dorit de iterații
	ale algoritmului.

	Pentru a juca contra calculatorului, rulați din modulul `Interactive`:

	> humanVsAI step
-}
step :: Board -> StdGen -> (Cell, Board)
step = undefined
